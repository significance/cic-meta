import * as http from 'http';
import * as fs from 'fs';
//import * as pg from 'pg';
import * as pgp from 'openpgp';
import * as sqlite from 'sqlite3';

import * as handlers from './handlers';
import { Envelope, Syncable } from '../src/sync';
import { PGPKeyStore, PGPSigner } from '../src/auth';

// TODO: Temporary code, privatekeys must not be read from file, of course
let serverAddr:string = process.env.SERVER_HOST;
let serverPort:number = parseInt(process.env.SERVER_PORT, 10);

let sqlite_file = process.env.SQLITE_FILE
let publicKeysTrustedActiveFile = process.env.PUBLICKEYS_TRUSTED_ACTIVE;
let publicKeysTrustedInactiveFile = process.env.PUBLICKEYS_TRUSTED_INACTIVE;
let publicKeysEncryptFile = process.env.PUBLICKEYS_ENCRYPT;
let privateKeyFile = process.env.PRIVATEKEY;
let privateKeyPassphrase = process.env.PRIVATEKEY_PASSPHRASE;
let dbConfig = {
	user: process.env.DATABASE_USER,
	password: process.env.DATABASE_PASSWORD,
	host: process.env.DATABASE_HOST,
	port: process.env.DATABASE_PORT,
	database: process.env.DATABASE_NAME,
};

serverAddr = '0.0.0.0';
serverPort = 7777;
dbConfig.host = 'localhost';
dbConfig.port = '5432';
dbConfig.database = 'cic-meta';
dbConfig.user  = 'postgres';
dbConfig.password = 'password';
sqlite_file = '/tmp/cicmeta.sqlite'
publicKeysTrustedActiveFile = __dirname + '/../tests/publickeys.asc';
privateKeyFile = __dirname + '/../tests/privatekeys.asc';
privateKeyPassphrase = 'merman';

const pubksa = fs.readFileSync(publicKeysTrustedActiveFile, 'utf-8');
const pksa = fs.readFileSync(privateKeyFile, 'utf-8');

//const db = new pg.Pool(dbConfig);
const db = new sqlite.Database(sqlite_file)

let signer = undefined;
const keystore = new PGPKeyStore(privateKeyPassphrase, pksa, pubksa, pubksa, pubksa, () => {
	keysLoaded();
});

function keysLoaded() {
	signer = new PGPSigner(keystore);
	startServer();
}

function startServer() {
	http.createServer(processRequest).listen(serverPort);
}

const re_digest = /^\/([a-fA-F0-9]{64})\/?$/;
function parseDigest(url) {
	const digest_test = url.match(re_digest);
	if (digest_test === null) {
		throw 'invalid digest';	
	}
	return digest_test[1].toLowerCase();
}

async function processRequest(req, res) {
	let digest = undefined;

	if (!['PUT', 'GET', 'POST'].includes(req.method)) {
		res.writeHead(405, {"Content-Type": "text/plain"});
		res.end();
		return;
	}

	// raw put option, no helper
	try {
		digest = parseDigest(req.url);
	} catch(e) {
		res.writeHead(400, {"Content-Type": "text/plain"});
		res.end();
		return;
	}

	const mergeHeader = req.headers['x-cic-automerge'];
	let mod = req.method.toLowerCase() + ":automerge:";
	switch (mergeHeader) {
		case "client":
			mod += "client"; // client handles merges
			break;
		case "server":
			mod += "server"; // server handles merges
			break;
		default:
			mod += "none"; // merged object only (get only)
	}

	let data = '';
	req.on('data', (d) => {
		data += d;
	});
	req.on('end', async () => {
		console.debug('mode', mod);
		let content = '';
		let contentType = 'application/json';
		let r = false;
		try {
			switch (mod) {
				case 'put:automerge:client':
					r = await handlers.handleClientMergePut(data, db, digest, keystore, signer);
					if (r == false) {
						res.writeHead(403, {"Content-Type": "text/plain"});
						res.end();
						return;
					}
					break;

				case 'get:automerge:client':
					content = await handlers.handleClientMergeGet(db, digest, keystore);	
					break;

				case 'post:automerge:server':
					content = await handlers.handleServerMergePost(data, db, digest, keystore, signer);	
					break;

				case 'put:automerge:server':
					r = await handlers.handleServerMergePut(data, db, digest, keystore, signer);	
					if (r == false) {
						res.writeHead(403, {"Content-Type": "text/plain"});
						res.end();
						return;
					}
					break;
				//case 'get:automerge:server':
				//	content = await handlers.handleServerMergeGet(db, digest, keystore);	
				//	break;

				case 'get:automerge:none':
					content = await handlers.handleNoMergeGet(db, digest, keystore);	
					break;

				default:
					res.writeHead(400, {"Content-Type": "text/plain"});
					res.end();
					return;
			}
		} catch(e) {
			console.error('fail', mod, digest, e);
			res.writeHead(500, {"Content-Type": "text/plain"});
			res.end();
			return;
		}

		if (content === undefined) {
			res.writeHead(400, {"Content-Type": "text/plain"});
			res.end();
			return;
		}

		res.writeHead(200, {
			"Content-Type": contentType,
			"Content-Length": content.length,
		});
		res.write(content);
		res.end();
	});
}
