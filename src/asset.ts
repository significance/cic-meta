import * as Automerge from 'automerge';

import { ArgPair, Syncable } from './sync';


class User extends Syncable {

	firstName: 	string
	lastName:	string

	constructor(id:string, v:Object={}) {
		if (v['user'] === undefined) {
			v['user'] = {
				firstName: '',
				lastName: '',
			}
		}
		super(id, v);
	}

	public setName(firstName:string, lastName:string) {
		const fn = new ArgPair('user.firstName', firstName)
		const ln = new ArgPair('user.lastName', lastName)
		this.update([fn, ln], 'update name');
	}
}

export { User };
