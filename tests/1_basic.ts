import * as Automerge from 'automerge';
import assert = require('assert');

import { Dispatcher, toIndexKey, fromIndexKey } from '../src/dispatch';
import { User } from '../src/asset';
import { Syncable, ArgPair } from '../src/sync';

import { MockSigner, MockStore } from './mock';

describe('basic', () => {

	it('store', () => {
		const store = new MockStore('s');
		assert.equal(store.name, 's');

		const mockSigner = new MockSigner();
		const v = new Syncable('foo', {baz: 42});
		v.setSigner(mockSigner);
		store.put('foo', v);
		const one = store.get('foo').toJSON();
		const vv = new Syncable('bar', {baz: 666});
		vv.setSigner(mockSigner);
		assert.throws(() => {
			store.put('foo', vv)
		});
		store.put('foo', vv, true);
		const other = store.get('foo').toJSON();
		assert.notEqual(one, other);
		store.delete('foo');
		assert.equal(store.get('foo'), undefined);
	});

	it('add_doc_to_dispatcher', () => {
		const store = new MockStore('s');
		//const syncer = new MockSyncer();
		const dispatcher = new Dispatcher(store, undefined);
		const user = new User('foo'); 
		dispatcher.add(user.id, user);
		assert(dispatcher.isDirty());
	});

	it('dispatch_keyindex', () => {
		const s = 'foo';
		const k = toIndexKey(s);
		const v = fromIndexKey(k);
		assert.equal(s, v);
	});

	it('sync_merge', () => {
		const mockSigner = new MockSigner();
		const s = new Syncable('foo', {
			bar: 'baz',
		});
		s.setSigner(mockSigner);
		const changePair = new ArgPair('xyzzy', 42);
		s.update([changePair], 'ch-ch-cha-changes');
		assert.equal(s.m.data['xyzzy'], 42)
		assert.equal(s.m.data['bar'], 'baz')
		assert.equal(s.m['id'], 'foo')
		assert.equal(Automerge.getHistory(s.m).length, 2);
	});

	it('sync_serialize', () => {
		const mockSigner = new MockSigner();
		const s = new Syncable('foo', {
			bar: 'baz',
		});
		s.setSigner(mockSigner);
		const j = s.toJSON();
		const ss = Syncable.fromJSON(j);
		assert.equal(ss.m['id'], 'foo');
		assert.equal(ss.m['data']['bar'], 'baz');
		assert.equal(Automerge.getHistory(ss.m).length, 1);
	});

	it('sync_sign_and_wrap', () => {
		const mockSigner = new MockSigner();
		const s = new Syncable('foo', {
			bar: 'baz',
		});
		s.setSigner(mockSigner);
		s.onwrap = (e) => {
			const j = e.toJSON();
			const v = JSON.parse(j);
			assert.deepEqual(v.payload, e.o.payload);

		}
		s.sign();
	});

});
